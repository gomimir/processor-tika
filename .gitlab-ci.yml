.go-cache:
  variables:
    GOPATH: $CI_PROJECT_DIR/.go
    CACHE_FALLBACK_KEY: "master"
  before_script:
  - mkdir -p .go
  cache:
    key: "$CI_COMMIT_REF_SLUG"
    paths:
    - .go/pkg/mod/

.dind:
  image: registry.gitlab.com/ericvh/docker-buildx-qemu
  variables:
    DOCKER_HOST: tcp://docker:2375/
    DOCKER_DRIVER: overlay2
    # See https://github.com/docker-library/docker/pull/166
    DOCKER_TLS_CERTDIR: ""
  services:
  - name: docker:dind
    entrypoint: ["env", "-u", "DOCKER_HOST"]
    command: ["dockerd-entrypoint.sh"]
  # Prepare env. for all docker jobs
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - update-binfmts --enable
    - docker buildx create --driver docker-container --use
    - &image_tag_export if [ `echo $CI_COMMIT_REF_NAME | grep -E '^v[0-9]+((.[0-9]+)?.[0-9]+)?$'` ]; then export IMAGE_TAG=$CI_COMMIT_REF_NAME; export IS_VERSION_TAG=true; else export IMAGE_TAG=$CI_COMMIT_REF_SLUG; export IS_VERSION_TAG=false; fi

stages:
- build
- package
- release

# Build application binaries
build binaries:
  extends: .go-cache
  stage: build
  image: golang:1.16-buster
  script:
  - make release
  artifacts:
    paths:
    - bin/release
    expire_in: 1 week

# Build tika image when changed
tika image:
  only:
    changes:
    - tika/Dockerfile
    - tika/tika-config.xml
  except:
  - tags
  extends: .dind
  stage: package
  script:
  - docker buildx build --no-cache --pull --push --platform linux/amd64,linux/arm64 -t $CI_REGISTRY_IMAGE/tika:latest tika

docker images:
  extends: .dind
  cache: {}
  stage: package
  script:
  - docker buildx build --no-cache --pull --push --platform linux/amd64,linux/arm64 -t $CI_REGISTRY_IMAGE:$IMAGE_TAG -f Dockerfile.ci bin
  - if $IS_VERSION_TAG; then docker buildx imagetools create -t $CI_REGISTRY_IMAGE:latest $CI_REGISTRY_IMAGE:$IMAGE_TAG; fi

release artifacts:
  stage: package
  only:
  - /^v[0-9]+\.[0-9]+\.[0-9]+$/
  except:
  - branches
  cache: {}
  image: curlimages/curl:latest
  script:
  - &release_package_version export PACKAGE_VERSION=`echo $CI_COMMIT_REF_NAME | sed -E 's/^v([0-9]+((.[0-9]+)?.[0-9]+)?)/\1/'`
  - &release_package_registry_url PACKAGE_REGISTRY_URL="${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/mimir-processor-tika/${PACKAGE_VERSION}"
  - 'curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file bin/release/linux/amd64/mimir-processor-tika ${PACKAGE_REGISTRY_URL}/mimir-processor-tika-linux-amd64'
  - 'curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file bin/release/linux/arm64/mimir-processor-tika ${PACKAGE_REGISTRY_URL}/mimir-processor-tika-linux-arm64'

release:
  stage: release
  only:
  - /^v[0-9]+\.[0-9]+\.[0-9]+$/
  except:
  - branches
  cache: {}
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  script:
  - *image_tag_export
  - *release_package_version
  - *release_package_registry_url
  - |
    release-cli create --name "$CI_COMMIT_TAG" --tag-name $CI_COMMIT_TAG \
      --description "Docker image: $CI_REGISTRY_IMAGE:$IMAGE_TAG" \
      --assets-link "{\"name\":\"mimir-processor-tika-linux-amd64\",\"url\":\"${PACKAGE_REGISTRY_URL}/mimir-processor-tika-linux-amd64\"}" \
      --assets-link "{\"name\":\"mimir-processor-tika-linux-arm64\",\"url\":\"${PACKAGE_REGISTRY_URL}/mimir-processor-tika-linux-arm64\"}"
