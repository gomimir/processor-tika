package extractor

import "net/http"

type tikaOptions struct {
	// OCR Language
	OCRLanguage *string

	// PDF OCR Strategy (NO_OCR, OCR_AND_TEXT, OCR_ONLY)
	PDFOCRStrategy *string
}

type tikaTransport struct {
	r    http.RoundTripper
	opts tikaOptions
}

func (transport *tikaTransport) RoundTrip(r *http.Request) (*http.Response, error) {
	if r.URL.Path == "/meta" {
		r.Header.Add("Accept", "application/json")
	}
	if r.URL.Path == "/tika" {
		r.Header.Add("Accept", "text/plain")

		// Note: we are not using Header#Add for Tika headers because it would mess up the key
		// Go HTTP expects all headers to be case insensitive and converts the case to avoid ambiguity.
		// Tika on the other hand treats headers case sensitive and ignores headers with messed up case

		if transport.opts.OCRLanguage != nil {
			r.Header["X-Tika-OCRLanguage"] = append(r.Header["X-Tika-OCRLanguage"], *transport.opts.OCRLanguage)
		}

		if transport.opts.PDFOCRStrategy != nil {
			r.Header["X-Tika-PDFocrStrategy"] = append(r.Header["X-Tika-PDFocrStrategy"], *transport.opts.PDFOCRStrategy)
		}
	}

	return transport.r.RoundTrip(r)
}
