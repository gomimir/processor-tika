package extractor

import (
	"bytes"
	"context"
	"io"
	"io/ioutil"
	"net/http"
	"regexp"
	"strings"
	"time"

	"github.com/google/go-tika/tika"
	"github.com/rs/zerolog/log"
	mimir "gitlab.com/gomimir/processor"
)

func EnsureReady(ctx context.Context, tikaURL string, opts mimir.RetryOpts) error {
	return mimir.Retry(ctx, func(ctx context.Context, ri mimir.RetryInfo) error {
		ctx, cancel := context.WithTimeout(ctx, 5*time.Second)
		defer cancel()

		req, err := http.NewRequestWithContext(
			ctx,
			"OPTIONS",
			tikaURL,
			nil,
		)

		if err == nil {
			var res *http.Response
			res, err = http.DefaultClient.Do(req)

			if err == nil {
				if res.StatusCode == 200 {
					return nil
				}
			}
		}

		if ri.WillTryAgain {
			log.Warn().Int("try", ri.Try).Err(err).Msgf("Unable to contact Tika server. Will try again in %v.", ri.NextCooldown)
		}

		return err
	}, opts.ApplyDefaults())
}

type ExtractOpts struct {
	// url is the URL of the Tika Server, including the port (if necessary), but
	// not the trailing slash. For example, http://localhost:9998.
	TikaURL string

	// optional language hint for OCR
	OCRLanguage *string

	Context context.Context
}

var pdfSuffixRx = regexp.MustCompile(`(?i)\.pdf$`)

func ExtractText(filename string, input io.Reader, opts ExtractOpts) (string, error) {
	if opts.Context == nil {
		opts.Context = context.TODO()
	}

	if pdfSuffixRx.MatchString(filename) {
		data, err := ioutil.ReadAll(input)
		if err != nil {
			return "", err
		}

		ocrStrategy := "NO_OCR"
		text, err := extractTextWithOptions(opts.Context, opts.TikaURL, bytes.NewReader(data), tikaOptions{PDFOCRStrategy: &ocrStrategy})
		if err == nil && len(text) > 0 {
			return text, err
		}

		ocrStrategy = "OCR_ONLY"
		return extractTextWithOptions(opts.Context, opts.TikaURL, bytes.NewReader(data), tikaOptions{PDFOCRStrategy: &ocrStrategy, OCRLanguage: opts.OCRLanguage})
	} else {
		return extractTextWithOptions(opts.Context, opts.TikaURL, input, tikaOptions{OCRLanguage: opts.OCRLanguage})
	}
}

func extractTextWithOptions(ctx context.Context, tikaURL string, input io.Reader, opts tikaOptions) (string, error) {
	httpClient := &http.Client{
		Timeout: time.Minute * 5, //5 minutes max to process documents.
		Transport: &tikaTransport{
			r:    http.DefaultTransport,
			opts: opts,
		},
	}

	tikaClient := tika.NewClient(httpClient, tikaURL)
	text, err := tikaClient.Parse(ctx, input)
	if err != nil {
		return "", err
	}

	return strings.TrimSpace(text), nil
}
