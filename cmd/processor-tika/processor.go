package main

import (
	"context"
	"fmt"
	"os"

	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	mimir "gitlab.com/gomimir/processor"
	"gitlab.com/gomimir/processor-tika/internal/extractor"
	mimirapp "gitlab.com/gomimir/processor/pkg/app"
	mimirpb "gitlab.com/gomimir/processor/protobuf"
	mimirpbconv "gitlab.com/gomimir/processor/protobufconv"
)

type fileRef struct {
	Repository string
	Filename   string
}

type taskConfig struct {
	OCRLanguage *string `mapstructure:"ocrLanguage"`
}

type taskParams struct {
	File   fileRef
	Config taskConfig
}

// populated by build flags
var version string

func main() {
	app := mimirapp.NewProcessorApp("mimir-processor-tika", version)
	rfp := app.RegisterFileProcessor("extractText", handler)
	rfp.OnCommandPreRun(func(cmd *cobra.Command) {
		ensureTikaReady(cmd.Context())
	})

	setTikaCmd(app.ProcessCommand())
	setTikaCmd(rfp.TestCommand())

	app.ProcessCommand().Flag("queue").DefValue = "extractText"
	viper.SetDefault("queue", "extractText")

	app.Execute()
}

func handler(req mimirapp.FileProcessorRequest) error {
	params := &taskParams{}

	if !req.IsTest {
		err := req.Task.Parameters().LoadTo(params)
		if err != nil {
			req.Log.Error().Err(err).Msg("Unable to parse task params")
			return err
		}
	}

	opts := getExtractorOpts(req.Command.Context())
	if params.Config.OCRLanguage != nil {
		opts.OCRLanguage = params.Config.OCRLanguage
	}

	reader, err := req.Task.File().Reader()
	if err != nil {
		req.Log.Error().Err(err).Msg("Unable read file")
		return err
	}

	defer reader.Close()

	text, err := extractor.ExtractText(req.Task.File().FileRef().Filename, reader, opts)

	if err != nil {
		req.Log.Error().Err(err).Msg("Unable to extract text")
		return err
	}

	if !req.IsTest {
		convertedProps, err := mimirpbconv.PropertiesToProtoBuf(*mimir.PropertiesFromMap(map[string]interface{}{
			"extractedText": text,
		}))
		if err != nil {
			req.Log.Error().Err(err).Msg("Failed to process asset properties")
			return err
		}

		_, err = req.Client.UpdateAssets(req.Command.Context(), &mimirpb.UpdateAssetsRequest{
			Selector: &mimirpb.UpdateAssetsRequest_Ref{
				Ref: &mimirpb.AssetRef{
					Index:   req.Task.File().AssetRef().IndexName(),
					AssetId: req.Task.File().AssetRef().AssetID(),
				},
			},
			UpdateFiles: []*mimirpb.UpdateAssetFileRequest{
				{
					Selector: &mimirpb.UpdateAssetFileRequest_Ref{
						Ref: &mimirpb.FileRef{
							Repository: req.Task.File().FileRef().Repository,
							Filename:   req.Task.File().FileRef().Filename,
						},
					},
					SetProperties: convertedProps,
				},
			},
		})

		if err != nil {
			req.Log.Error().Err(err).Msg("Unable to set file properties")
			return err
		}
	} else {
		fmt.Println(text)
	}

	return nil
}

func setTikaCmd(cmd *cobra.Command) {
	cmd.PersistentFlags().String("ocr-language", "", "OCR language hint")
	cmd.PersistentFlags().String("tika-url", "http://localhost:9998", "URL to running Apache Tika server")
	viper.BindPFlags(cmd.PersistentFlags())
}

func ensureTikaReady(ctx context.Context) {
	tikaURL := viper.GetString("tika-url")
	err := extractor.EnsureReady(ctx, tikaURL, mimir.RetryOpts{
		MaxTries: -1,
	})

	if err != nil {
		log.Fatal().Str("url", tikaURL).Err(err).Msg("Tika server is not available")
		os.Exit(1)
	}

	log.Info().Str("url", tikaURL).Msg("Tika server is up and running")
}

func getExtractorOpts(ctx context.Context) extractor.ExtractOpts {
	opts := extractor.ExtractOpts{
		Context: ctx,
		TikaURL: viper.GetString("tika-url"),
	}

	ocrLanguage := viper.GetString("ocr-language")
	if ocrLanguage != "" {
		opts.OCRLanguage = &ocrLanguage
	}

	return opts
}
