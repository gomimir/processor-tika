module gitlab.com/gomimir/processor-tika

go 1.16

require (
	github.com/google/go-tika v0.1.21 // indirect
	github.com/rs/zerolog v1.20.0 // indirect
	gitlab.com/gomimir/processor v0.28.0 // indirect
	golang.org/x/net v0.0.0-20210326220855-61e056675ecf // indirect
)

// replace gitlab.com/gomimir/processor => ../processor