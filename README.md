# Mimir text extractor

Text extractor is a Mimir processor that extracts plain text content from indexed documents.

For more information about how to use it see [documentation](https://gomimir.gitlab.io/processors/text-extractor).

## Development

The processor uses Apache Tika for extraction. First start your Tika server.

```bash
docker-compose up -d
```

Make a test run on some of your local files:

```bash
go run cmd/processor-tika/processor.go test extractText your_test_file.pdf
```

Test it together with the rest of the platform (expects you have Mimir + Redis running on your localhost):

```bash
make run
```

Make a docker image to test your work in deployed environment:

```bash
make docker
```


