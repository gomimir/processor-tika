FROM golang:1.16-buster AS build

ADD cmd /app/cmd
ADD internal /app/internal
ADD go.mod /app/
ADD go.sum /app/

WORKDIR /app
ARG VERSION
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -ldflags "-X main.version=$VERSION" -o ./bin/mimir-processor-tika ./cmd/processor-tika/processor.go

FROM gcr.io/distroless/static
COPY --from=build /app/bin/mimir-processor-tika /mimir-processor-tika
CMD ["/mimir-processor-tika", "process"]